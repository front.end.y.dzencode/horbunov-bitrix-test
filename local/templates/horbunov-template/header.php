<!DOCTYPE html>
<html>
<head>
    <!-- partial:parts/_head.html -->
    <meta charset="utf-8">
    <title>BASIC-STATIC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"><!-- responsive -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="<?= SITE_TEMPLATE_PATH ?>/style.css" type="text/css" rel="stylesheet"/>
    <link href="../favicon.png" rel="shortcut icon" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,300,400,500,700,900&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:400,600,700&display=swap" rel="stylesheet">
    <!-- partial -->

    <?
    //region ShowHead
    echo '<!-- ShowHead -->';
    $APPLICATION->ShowHead();
    echo '<!-- /ShowHead -->';
    //endregion ShowHead
    ?>
</head>
<body>


<? //region showPanel
if ($USER->IsAdmin()):
    ?>
    <div id="panel">
        <?
        $APPLICATION->ShowPanel();
        ?>
    </div>
<?endif;
//endregion showPanel?>


<div id="wrapper">
    <!-- partial:parts/_header.html -->
    <header id="header">
        <div class="container">
            <div class="header-content">
                <!-- logo -->
                <div class="logo">

                    <a href="index.html">
                        <img class="blue" src="images/scroll-logo.png" alt="logo"/>
                        <img class="white" src="images/logo.png" alt="logo"/>
                    </a>
                </div>
                <div class="settings-wrap">
                    <span class="btn touch">Get in touch</span>
                    <a href="tel: 123456789" class="kontakt-phone"><i class="fa fa-phone" aria-hidden="true"></i>
                    </a>
                    <div class="burger menu-opener-js">
                        <div><span class="bar"></span><span class="bar"></span><span
                                    class="bar"></span></div>
                    </div>
                    <div class="language">
                        <ul>
                            <li class="active"><a href="#">DE</a></li>
                            <li><a href="#">EN</a></li>
                        </ul>
                    </div>

                </div><!-- //settings-wrap -->
            </div><!-- //header -->


            <div class="setting-section">
                <div class="row">
                    <div class="col col-6 col-mob-12 navigation">
                        <span class="nav-title">VON DER IDEE ZUR SERIE</span>
                        <nav>
                            <ul>
                                <li class="active"><a href="index.html">HOME</a></li>
                                <li><a href="uber-uns.html">Über uns</a></li>
                                <li><a href="unsere-mitglieder.html">Unsere Mitglieder</a></li>
                                <li><a href="werkzeuge.html">Werkzeuge</a></li>
                                <li><a href="community.html">Community</a></li>
                                <li><a href="team.html">Team</a></li>
                                <li><a href="rooms.html">ROOMS</a></li>
                                <li><a href="kontakt.html">Kontakt</a></li>
                            </ul>
                        </nav>
                    </div> <!-- col -->
                    <div class="col col-6 col-mob-12">
                        <div class="kontakt-wrap">


                            <div class="kontakt">
                                <h4>Kontakt</h4>
                                <span class="address">
                    Motionlab berlin<br>
                    bouchestrasse 12, Hallo 20<br>
                    12435 Berlin
                </span>
                            </div> <!-- kontakt -->
                            <div class="setting-links">
                                <ul class="nav-list">
                                    <li><a href="impressum.html">Impressum</a></li>
                                    <li><a href="datenschutz.htmldate">Datenschutzerklärung</a></li>
                                    <li><a href="#">AGBs</a></li>
                                </ul>
                                <ul class="social-list">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                                </ul>
                            </div><!-- //setting-links -->
                        </div>
                    </div> <!-- col -->
                </div> <!-- row -->


            </div><!-- //setting-section -->

        </div><!-- //container -->

    </header>

    <!-- partial -->


    <main id="middle" class="content-scroller">
